IMAGE := quay.io/zachpodbielniak/workstation
CURRENT := 40

ifndef $(VERSION)
	VERSION = $(CURRENT)
endif

ifndef $(TAG)
	TAG = $(CURRENT)
endif


.PHONY: all all_upgrade build push rebase build_build push_build


all: build push
all_upgrade: all update


build:
	buildah build --no-cache -t $(IMAGE):$(TAG) -f ./Containerfile --build-arg=FEDORA_VERSION=$(VERSION)

push:
	buildah push $(IMAGE):$(TAG)

iso:
	mkdir -p ./iso
	sudo podman run --rm --privileged --volume ./iso:/build-container-installer/build  ghcr.io/jasonn3/build-container-installer:latest VERSION=40 IMAGE_NAME=workstation IMAGE_TAG=40 IMAGE_REPO=quay.io/zachpodbielniak IMAGE_SIGNED=false VARIANT=Silverblue





upgrade:
	sudo rpm-ostree update

rebase:
	sudo rpm-ostree rebase ostree-unverified-registry:$(IMAGE):$(TAG)



build_build:
	buildah build --no-cache -t $(IMAGE):build -f ./build_container/Containerfile ./build_container

push_build:
	buildah push $(IMAGE):build

