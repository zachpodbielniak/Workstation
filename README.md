# Workstation

My build of a fedora silverblue workstation.

## To install:
- Install any recent version of Fedora silverblue
- Let it update (check `rpm-ostree status` until its done)
- Run: `sudo rpm-ostree rebase ostree-unverified-registry:quay.io/zachpodbielniak/workstation:40` and reboot after its done. 
- After reboot: `/etc/workstation/scripts/install.sh`
- Enjoy :) 

## To update:
- `sudo rpm-ostree update`
- Reboot 
- `/etc/workstation/scripts/update.sh`

## To build:
- `make TAG=40 BUILD=40 all`

