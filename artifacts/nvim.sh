#!/bin/bash 

in_container () { 
	[ -f /run/.containerenv ] && return 1 || return 0
}


get_current_container_name () {
	local in_c=$(in_container)
	if [ "1" == "$in_c" ] 
	then
		cat /run/.containerenv | grep -i name | awk -F= '{printf "%s\n", $2}' | sed 's/\"//g'
	else
		echo "0"
	fi
}

if [ $(( in_container )) -eq 1 ]
then

    if [ get_current_container_name == "dev" ]
    then 
        exec /usr/bin/nvim $@
    else 
        exec /usr/local/bin/nvim $@
    fi
else 
    exec /usr/bin/nvim $@
fi

