#!/bin/bash
git clone https://gitlab.com/zachpodbielniak/Workstation.git

buildah login $REGISTRY -u $USER -p $PASS
[ $? -ne 0 ] && exit 1

buildah build --no-cache -t "$REGISTRY/zachpodbielniak/workstation:$VERSION" -f ./Workstation/Containerfile --build-arg=FEDORA_VERSION=$VERSION ./Workstation 
[ $? -ne 0 ] && exit 1

buildah push "$REGISTRY/zachpodbielniak/workstation:$VERSION"
[ $? -ne 0 ] && exit 1

if [ "$SET_AS_LATEST" == "1" ]  
then
	buildah tag "$REGISTRY/zachpodbielniak/workstation:$VERSION" "$REGISTRY/zachpodbielniak/workstation:latest"
	[ $? -ne 0 ] && exit 1

	buildah push "$REGISTRY/zachpodbielniak/workstation:latest"
	[ $? -ne 0 ] && exit 1
fi

exit 0
