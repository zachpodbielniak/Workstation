#!/bin/bash
source ./functions.sh

NAME="dev"

check_container_or_die $NAME

PACKAGES=()
PACKAGES+=("gdb")
PACKAGES+=("python3-pip")
PACKAGES+=("neovim")
PACKAGES+=("nodejs")
PACKAGES+=("R")
PACKAGES+=("clang")
PACKAGES+=("clang-tools-extra")
PACKAGES+=("bear")
PACKAGES+=("code")
PACKAGES+=("json-glib-devel")
PACKAGES+=("nautilus-devel")
PACKAGES+=("nautilus-extensions")
PACKAGES+=("gtk4-devel")
PACKAGES+=("tig")
PACKAGES+=("fira-code-fonts")
PACKAGES+=("bash-completion")
PACKAGES+=("kubernetes-client")
PACKAGES+=("helm")
PACKAGES+=("ffmpeg")
PACKAGES+=("bpftool")
PACKAGES+=("libbpf")
PACKAGES+=("libbpf-devel")
PACKAGES+=("libbpf-tools")
PACKAGES+=("bcc-tools")
PACKAGES+=("home-assistant-cli")


PIP_PACKAGES=()
# See https://github.com/Freed-Wu/autotools-language-server/issues/9
PIP_PACKAGES+=("tree-sitter==0.21.3")
PIP_PACKAGES+=("autotools-language-server")

NPM_PACKAGES=()
NPM_PACKAGES+=("bash-language-server")
NPM_PACKAGES+=("dockerfile-language-server-nodejs")
NPM_PACKAGES+=("pyright")


BIN_EXPORTS=()
BIN_EXPORTS+=("code")
BIN_EXPORTS+=("tig")
BIN_EXPORTS+=("kubectl")
BIN_EXPORTS+=("helm")
BIN_EXPORTS+=("hass-cli")
BIN_EXPORTS+=("autotools-language-server")
BIN_EXPORTS+=("bash-language-server")
BIN_EXPORTS+=("docker-langserver")
BIN_EXPORTS+=("pyright-langserver")
BIN_EXPORTS+=("clangd")
BIN_EXPORTS+=("R")
BIN_EXPORTS+=("Rscript")


APP_EXPORTS=()
APP_EXPORTS+=("code")


BIN_SYMLINK=()
BIN_SYMLINK+=("podman")
BIN_SYMLINK+=("buildah")
BIN_SYMLINK+=("distrobox")
BIN_SYMLINK+=("strace")
BIN_SYMLINK+=("ltrace")
BIN_SYMLINK+=("bpftrace")
BIN_SYMLINK+=("rg")


# Update
sudo dnf update -y

# Setup VS Code Repo - https://code.visualstudio.com/docs/setup/linux
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'


# Setup ffmpeg for supporting MTP - https://computingforgeeks.com/how-to-install-ffmpeg-on-fedora/
sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf -y install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm


# @development-tools grouping
sudo dnf install -y @development-tools


# Install all packages
sudo dnf install -y ${PACKAGES[@]}


#Install pip packages
sudo pip3 install ${PIP_PACKAGES[@]}


# Install NPM packages
sudo npm i -g ${NPM_PACKAGES[@]}


# Export binaries
for bin in "${BIN_EXPORTS[@]}"
do 
    bin_export $bin
done


# Export apps
for app in "${APP_EXPORTS[@]}"
do
    app_export $app
done


for bin in "${BIN_SYMLINK[@]}"
do
    sudo ln -s /usr/bin/distrobox-host-exec "/usr/local/bin/$bin"
done

# For R we need to do:
# sudo R 
# install.packages("languageserver")
