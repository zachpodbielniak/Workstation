#!/bin/bash
source ./functions.sh

NAME="ffmpeg"

check_container_or_die $NAME

PACKAGES=()
PACKAGES+=("ffmpeg")


BIN_EXPORTS=()
BIN_EXPORTS+=("ffmpeg")


APP_EXPORTS=()


# Update
sudo dnf update -y

# Setup FFMpeg with non-frees - https://computingforgeeks.com/how-to-install-ffmpeg-on-fedora/
sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf -y install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm


# Install all packages
sudo dnf install -y ${PACKAGES[@]}


# Export binaries
for bin in "${BIN_EXPORTS[@]}"
do 
    bin_export $bin
done


# Export apps
for app in "${APP_EXPORTS[@]}"
do
    app_export $app
done

