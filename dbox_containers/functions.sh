#!/bin/bash

check_container_or_die() {
    [ $# -ne 1 ] && echo "check_container() requires one parameter for the name" && exit 1
    local name="$1"

    [ ! -f /run/.containerenv ] && echo "This is not a container!" && exit 1
    dbox_name=$(cat /run/.containerenv | grep -P "name=" | awk -F\" '{printf "%s\n", $2}')

    [ "$name" != "$dbox_name" ] && echo "Configured name \"$name\" does not match containers name of \"$dbox_name\"" && exit 1
}


bin_install_if_not_found () {
    [ $# -ne 2 ] && echo "bin_install_if_not_found() requires two parameters for the bin path and then package to install" && exit 1
    local bin="$1" 
    local package="$2"

    [ ! -f $bin ] && sudo dnf install -y $package
}


make_export () {
    [ "$#" != 1 ] && echo "$0 <bin>" && exit 1
    [ -f "~/bin/export/$1" ] && rm "~/bin/export/$1" 
    distrobox-export --bin $(which "$1") --export-path ~/bin/export/
}


make_app () {
    [ "$#" != 1 ] && echo "$0 <app>" && exit 1
    distrobox-export --app "$1"
}


bin_export () {
    [ $# -ne 1 ] && echo "bin_export() requires one parameter for the bin-name" && exit 1
    local bin="$1"

    # If which doesn't exist install it
    bin_install_if_not_found /usr/bin/which which

    echo "Exporting executable \"$bin\" to host"
    make_export $bin
}


app_export () {
    [ $# -ne 1 ] && echo "app_export() requires one parameter for the app-name" && exit 1
    local app="$1"

    echo "Exporting app \"$app\" to host"
    make_app $app
}
