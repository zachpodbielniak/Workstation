#!/bin/bash
source ./functions.sh

NAME="mpd"

check_container_or_die $NAME

PACKAGES=()
PACKAGES+=("which")
PACKAGES+=("mpd")
PACKAGES+=("ncmpcpp")


BIN_EXPORTS=()
BIN_EXPORTS+=("mpd")
BIN_EXPORTS+=("ncmpcpp")


APP_EXPORTS=()


BIN_SYMLINK=()
BIN_SYMLINK+=("podman")
BIN_SYMLINK+=("buildah")
BIN_SYMLINK+=("distrobox")
BIN_SYMLINK+=("strace")
BIN_SYMLINK+=("ltrace")
BIN_SYMLINK+=("bpftrace")
BIN_SYMLINK+=("rg")


# Update
sudo dnf update -y


# Install free fusion
sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm


# Install all packages
sudo dnf install -y ${PACKAGES[@]}



# Export binaries
for bin in "${BIN_EXPORTS[@]}"
do 
    bin_export $bin
done


# Export apps
for app in "${APP_EXPORTS[@]}"
do
    app_export $app
done


for bin in "${BIN_SYMLINK[@]}"
do
    sudo ln -s /usr/bin/distrobox-host-exec "/usr/local/bin/$bin"
done

