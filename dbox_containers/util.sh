#!/bin/bash
source ./functions.sh

NAME="util"

check_container_or_die $NAME

PACKAGES=()
PACKAGES+=("calc")
PACKAGES+=("htop")
PACKAGES+=("ncdu")
PACKAGES+=("neofetch")
PACKAGES+=("ranger")
PACKAGES+=("xfreerdp")
PACKAGES+=("glances")
PACKAGES+=("pandoc")
PACKAGES+=("stow")


BIN_EXPORTS=()
BIN_EXPORTS+=("calc")
BIN_EXPORTS+=("htop")
BIN_EXPORTS+=("ncdu")
BIN_EXPORTS+=("neofetch")
BIN_EXPORTS+=("ranger")
BIN_EXPORTS+=("xfreerdp")
BIN_EXPORTS+=("glances")
BIN_EXPORTS+=("pandoc")
BIN_EXPORTS+=("stow")



APP_EXPORTS=()


BIN_SYMLINK=()
BIN_SYMLINK+=("podman")
BIN_SYMLINK+=("buildah")
BIN_SYMLINK+=("distrobox")
BIN_SYMLINK+=("strace")
BIN_SYMLINK+=("ltrace")
BIN_SYMLINK+=("bpftrace")
BIN_SYMLINK+=("nvim")
BIN_SYMLINK+=("rg")


# Update
sudo dnf update -y


# Install all packages
sudo dnf install -y ${PACKAGES[@]}


# Export binaries
for bin in "${BIN_EXPORTS[@]}"
do 
    bin_export $bin
done


# Export apps
for app in "${APP_EXPORTS[@]}"
do
    app_export $app
done


for bin in "${BIN_SYMLINK[@]}"
do
    sudo ln -s /usr/bin/distrobox-host-exec "/usr/local/bin/$bin"
done
