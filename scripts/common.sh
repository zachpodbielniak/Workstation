#!/bin/bash


declare -A CONTAINERS=(
	[dev]="registry.fedoraproject.org/fedora:40"
	[util]="registry.fedoraproject.org/fedora:40"
	[ffmpeg]="registry.fedoraproject.org/fedora:39"
    [mpd]="registry.fedoraproject.org/fedora:40"
)


flatpak_config () {
	# Remove flathub if its configured
	flatpak remote-delete flathub --force


	# Enabling flathub (unfiltered) for --user
	flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo


	# Replace Fedora flatpaks with flathub ones
	flatpak install --user --noninteractive org.gnome.Platform//46
	flatpak install --user --noninteractive --reinstall flathub $(flatpak list --app-runtime=org.fedoraproject.Platform --columns=application | tail -n +1 )


	# Remove system flatpaks (pre-installed)
	flatpak remove --system --noninteractive --all


	# Remove Fedora flatpak repo
	flatpak remote-delete fedora --force
}


flatpak_install () {
	# Install flatpaks shipped from the packages.yml
	[ ! -f /etc/workstation/packages.yml ] && echo "/etc/workstation/packages.yml does not exist!" && exit 1
	flatpaks=$(yq '.flatpaks[]' < /etc/workstation/packages.yml)
	for flatpak in $flatpaks; do flatpak --user --noninteractive install $flatpak; done
}


update_nvim_config () {
    if [ -d "$HOME/.config/nvim" ]
    then
        sh -c "cd $HOME/.config/nvim && git pull"
    fi
}


install_artifacts_systemd_user () {
  mkdir -p ~/.config/systemd/user 

  for f in ../artifacts/systemd/user/*.server 
  do
      cat "$f" | sed "s/<<USER>>/$USER/g" > "~/.config/systemd/user/$(basename $f)"
  done

  systemctl --user daemon-reload

  for f in ../artifacts/systemd/user/*.service 
  do 
    # -8 here since '.service' is 8 chars long
    local service_name="${f:0:-8}"
    systemctl --user enable $service_name
  done

}


install_artifacts_systemd_system () {
    sudo cp ../artifacts/systemd/system/*.service /etc/systemd/system/
    sudo systemctl daemon-reload
  
    for f in ../artifacts/systemd/system/*.service 
    do 
        # -8 here since '.service' is 8 chars long
        local service_name="${f:0:-8}"
        sudo systemctl enable $service_name
    done
}


install_artifacts_quadlet_user () {
    local user_dir="$HOME/.config/containers/systemd"

    echo "Creating directory for Quadlet User: $user_dir"
    mkdir -p $user_dir

    for container in ../artifacts/quadlet/user/*.container 
    do 
        local filename=$(basename "$container")

        if [ "$filename" == "non-existent.container" ]
        then 
            echo "Should not happen, just here for container specific install instructions"
        else
            cat "$container" | sed "s/<<USER>>/$USER/g" > "$user_dir/$filename"
        fi
    done
}


install_artifacts_quadlet_system () {
    local system_dir="/etc/containers/systemd"

    echo "Creating directory for Quadlet User: $system_dir"
    sudo mkdir -p $system_dir

    for container in ../artifacts/quadlet/system/*.container 
    do 
        local filename=$(basename "$container")

        if [ "$filename" == "non-existent.container" ]
        then 
            echo "Should not happen, just here for container specific install instructions"
        else
            sudo cp "$container" "$user_dir/$filename"
        fi
    done
}


install_artifacts_systemd () {
    install_artifacts_systemd_user
    install_artifacts_systemd_system
    install_artifacts_quadlet_user
    install_artifacts_quadlet_system

    echo "Reloading systemd daemons for quadlet containers"
    sudo systemctl daemon-reload
    systemctl --user daemon-reload
}




install_artifacts_starship () {
	mkdir -p "$HOME/bin/starship"
	curl -Lo /tmp/install_starship.sh https://starship.rs/install.sh
	sh /tmp/install_starship.sh -y -b "$HOME/bin/starship/"
	rm /tmp/install_starship.sh
}


install_artifacts_bashrc () {
    local is_installed=$(grep "WORKSTATION_BASHRC" ~/.bashrc)

    # do install if its empty
    if [ "$is_installed" == "" ]
    then
        echo "# WORKSTATION_BASHRC" >> ~/.bashrc
        echo "source ~/.bashrc-workstation" >> ~/.bashrc
        cp ../artifacts/bashrc-workstation ~/.bashrc-workstation
    fi
}


install_artifacts_nvim_wrapper () {
    mkdir -p "$HOME/bin/scripts"
    cp ../artifacts/nvim.sh "$HOME/bin/scripts/nvim"
}


install_artifacts_nvim_config () {
    if [ -d "$HOME/.config/nvim" ]
    then
        mv "$HOME/.config/nvim" "$HOME/.config/nvim.bk"
    fi

    if [ -d "$HOME/.local/nvim" ]
    then 
        rm -rf "$HOME/.local/nvim"
    fi

    git clone https://gitlab.com/zachpodbielniak/nvim "$HOME/.config/nvim"
}


install_artifacts_fonts () {
    declare -a fonts=(
	    Agave
	    AnonymousPro
	    Arimo
	    AurulentSansMono
	    BigBlueTerminal
	    BitstreamVeraSansMono
	    CascaidaCode
	    CodeNewRoman
	    Cousine
	    DaddyTimeMono
	    DejaVuSansMono
	    DroidSansMono
	    FantasqueSansMono
	    FiraCode
	    FiraMono
	    Go-Mono
	    Gohu
	    Hack
	    Hasklig
	    HeavyData
	    Hermit
	    iA-Writer
	    IBMPlexMono
	    Inconsolate
	    InconsolataGo
	    InconsolataLGC
	    Iosevka
	    JetBrainsMono
	    Lekton
	    LiberationMono
	    Lilex
	    Meslo
	    Monofur
	    Mononoki
	    Monoid
	    MPlus
	    NerdFontsSymbolsOnly
	    Noto
	    OpenDyslexic
	    Overpass
	    ProFont
	    ProggyClean
	    RobotoMono
	    ShareTechMono
	    Terminus
	    Tinos
	    Ubuntu
	    UbuntuMono
	    VictorMono
    )

    version='2.2.2'
    fonts_dir="${HOME}/.local/share/fonts"

    if [[ ! -d "$fonts_dir" ]]; then
        mkdir -p "$fonts_dir"
    fi

    for font in "${fonts[@]}"; do
        zip_file="${font}.zip"
        download_url="https://github.com/ryanoasis/nerd-fonts/releases/download/v${version}/${zip_file}"
        echo "Downloading $download_url"
        curl -Lo "/tmp/${zip_file}" "$download_url"
        unzip "$zip_file" -d "$fonts_dir" -x "*.txt/*" -x "*.md/*" -o
        rm "/tmp/${zip_file}"
    done

    find "$fonts_dir" -name '*Windows Compatible*' -delete

    fc-cache -fv
}


# Note, does not do install_artifacts_fonts
install_artifacts_all () {
    install_artifacts_systemd
	install_artifacts_starship
	install_artifacts_bashrc
    install_artifacts_nvim_config
    install_artifacts_nvim_wrapper
}
