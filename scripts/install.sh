#!/bin/bash

# Make sure this is ran in the scripts dir
current_folder=$(dirname $0)
if [ "." != $current_folder ]
then 
    cd "$current_folder"
fi

source ./common.sh

flatpak_config
flatpak_install


# Mkdir for the exported bins
mkdir -p ~/bin/export

# cd to where the dbox_container init scripts are 
cd /etc/workstation/dbox_containers


for container in "${!CONTAINERS[@]}"
do 
	distrobox-create --yes -i "${CONTAINERS[$container]}" -n "$container" &
done
wait

for container in "${!CONTAINERS[@]}"
do 
	distrobox-enter "$container" -- ./$container.sh &
done




# Disable tracker3 (uses a lot of battery and CPU power)
systemctl --user mask \
	tracker-extract-3.service \
	tracker-miner-fs-3.service \
	tracker-miner-rss-3.service \
	tracker-writeback-3.service \
	tracker-xdg-portal-3.service \
	tracker-miner-fs-control-3.service


install_artifacts_all &
install_artifacts_fonts &
wait

