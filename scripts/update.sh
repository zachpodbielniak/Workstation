#!/bin/bash

# Make sure this is ran in the scripts dir
current_folder=$(dirname $0)
echo "$current_folder"
if [ "." != $current_folder ]
then 
    cd "$current_folder"
fi


source ./common.sh

[ -f /run/.containerenv ] && echo "$0 can not be ran in a container!" && exit 1


get_containers () {
    local container_listing=$(distrobox list --no-color)

    while read -r line
    do
        [ "$line" != "NAME" ] && awk '{printf "%s\n", $3}'
    done <<< $container_listing
}


container_exists () {
    local to_check="$1"
    local containers=$(get_containers)
    local exists=0

    while read -r line
    do 
        [ "$to_check" == "$line" ] && exists=1 
    done <<< $containers

    echo $exists
}


cd ../dbox_containers


for container in "${!CONTAINERS[@]}"
do 
    if [ 0 -eq $(container_exists $container) ]
    then
        echo "Will create $container"
    	distrobox-create --yes -i "${CONTAINERS[$container]}" -n "$container" &
    fi
done
wait

# Remove exported bins to not confuse PATH
rm ~/bin/export/*

for container in "${!CONTAINERS[@]}"
do 
    echo "Will update $container"
	distrobox-enter "$container" -- ./$container.sh &
done

flatpak_install &
install_artifacts_all &
update_nvim_config &

wait

